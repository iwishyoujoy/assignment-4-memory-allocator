#define _DEFAULT_SOURCE

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

bool test_1(){
    printf("TECT 1. Обычное успешное выделение памяти.\n");

    void* heap = heap_init(100);
    if(!heap){
        printf("ОШИБКА: Куча не была иницилизированна\n");
        return false;
    }

    debug_heap(stdout, heap);
    void* alloc = _malloc(10);

    if(!alloc){
        printf("ОШИБКА: Что-то пошло не так с malloc\n");
        return false;
    }

    debug_heap(stdout, heap);
    _free(alloc);

    munmap(HEAP_START, 8192);

    printf("ТЕСТ 1 пройден! Ты супер!\n\n");
    return true;
}

bool test_2(){
    printf("TECT 2. Освобождение одного блока из нескольких выделенных.\n");
    
    void* heap = heap_init(100);

    if(!heap){
        printf("ОШИБКА: Куча не была иницилизированна\n");
        return false;
    }

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10);
    void* alloc_2 = _malloc(20);

    if(!alloc_1 || !alloc_2){
        printf("ОШИБКА: Что-то пошло не так с malloc\n");
        return false;
    }

    debug_heap(stdout, heap);
    _free(alloc_1);
    debug_heap(stdout, heap);

    munmap(HEAP_START, 8192);
    printf("ТЕСТ 2 пройден! Ты умница!\n\n");
    return true;
}

bool test_3(){
    printf("TECT 3. Освобождение двух блоков из нескольких выделенных.\n");

    void* heap = heap_init(100);

    if(!heap){
        printf("ОШИБКА: Куча не была иницилизированна\n");
        return false;
    }

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10);
    void* alloc_2 = _malloc(20);
    void* alloc_3 = _malloc(30);

    if(!alloc_1 || !alloc_2 || !alloc_3){
        printf("ОШИБКА: Что-то пошло не так с malloc\n");
        return false;
    }

    debug_heap(stdout, heap);
    _free(alloc_1);
    debug_heap(stdout, heap);
    _free(alloc_3);
    debug_heap(stdout, heap);

    munmap(HEAP_START, 8192);
    printf("ТЕСТ 3 пройден! Так держать!\n\n");
    return true;
}

bool test_4(){
    printf("TECT 4. Память закончилась, новый регион памяти расширяет старый.\n");

    void* heap = heap_init(100);

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10000);

    if(!alloc_1){
        printf("ОШИБКА: Что-то пошло не так с malloc\n");
        return false;
    }

    debug_heap(stdout, heap);
    _free(alloc_1);

    munmap(heap, 20480);
    printf("ТЕСТ 4 пройден! Я тобой горжусь!\n\n");
    return true;
}

bool test_5(){
    printf("TECT 5. Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n");

    void* heap = heap_init(100);

    void* map = mmap(HEAP_START + 8192, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

    debug_heap(stdout, heap);

    void* alloc_1 = _malloc(10000);

    if(!alloc_1){
        printf("ОШИБКА: Что-то пошло не так с malloc\n");
        return false;
    }

    debug_heap(stdout, heap);

    _free(alloc_1);
    debug_heap(stdout, heap);

    munmap(map, 4096);
    printf("ТЕСТ 5 пройден! Я тебя обожаю! Ты самый крутой программист в этом мире!!\n\n");
    return true;
}
