#define ALL_DONE 5

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tests.h"

int main(void) {
    int result = 0;
    result += test_1();
    result += test_2();
    result += test_3();
    result += test_4();
    result += test_5();
    if (result == ALL_DONE){
        printf("Все тесты успешно пройдены! \n");
        return EXIT_SUCCESS;
    }
    else{
        printf("Было пройдено %d/5 тестов! Ты старался, но нужно что-то исправить! \n", result);
        return EXIT_FAILURE;
    }
}
